Port Summary
Port  Display         VLAN Name          Port  Link  Speed  Duplex
#     String          (or # VLANs)       State State Actual Actual
==================================================================
1                     v17                 E     A     1000  FULL 
2                     v17                 E     A     1000  FULL 
3                     v17                 E     A     1000  FULL 
4                     v17                 E     A     1000  FULL 
5                     v17                 E     A     1000  FULL 
6                     v17                 E     A     1000  FULL 
7                     v17                 E     A     1000  FULL 
8                     v17                 E     A     1000  FULL 
9                     (0002)              E     A     1000  FULL 
10                    v17                 E     R                            
11                    v17                 E     R                            
12                    v17                 E     R                            
13                    v17                 E     R                            
14                    v17                 E     R                            
15                    v17                 E     R                            
16                                        E     R                            
17                    (0002)              E     A     1000  FULL 
18                    v4_6                E     A     100   FULL 
19                    (0005)              E     A     1000  FULL 
20                    (0005)              E     A     1000  FULL 
21                    HKT-ToDC5           D     R                            
22                    v16                 E     A     1000  FULL 
23                    v16                 E     A     100   HALF 
24                    HKT-ToDC5           E     A     100   FULL 
25                                        E     NP                           
26                                        E     NP                           
==================================================================
   Port State: D-Disabled, E-Enabled
   Link State: A-Active, R-Ready, NP-Port not present, L-Loopback,
               D-ELSM enabled but not up
               d-Ethernet OAM enabled but not up


