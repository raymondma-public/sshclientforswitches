Port Summary
Port  Display         VLAN Name          Port  Link  Speed  Duplex
#     String          (or # VLANs)       State State Actual Actual
==================================================================
1                     HKT-ToDC5           D     R                            
2                     HKT-ToDC5           E     A     100   FULL 
3                     V8                  E     R                            
4                     V8                  E     R                            
5                     V8                  E     A     1000  FULL 
6                     V8                  E     A     100   FULL 
7                     V8                  E     R                            
8                     V8                  E     A     1000  FULL 
9                     V8                  E     A     100   FULL 
10                    V8                  E     R                            
11                    (0002)              E     R                            
12                    (0002)              E     R                            
13                    (0002)              E     R                            
14                    (0002)              E     R                            
15                    (0002)              E     R                            
16                    (0002)              E     R                            
17                    (0002)              E     R                            
18                    (0002)              E     R                            
19                    (0002)              E     R                            
20                    V249                E     A     1000  FULL 
21                    (0002)              E     R                            
22                    (0002)              E     R                            
23                    (0002)              E     R                            
24                    (0002)              E     R                            
25                    (0002)              E     R                            
26                    (0002)              E     R                            
27                    (0002)              E     R                            
28                    (0002)              E     R                            
29                    (0002)              E     R                            
30                    (0002)              E     R                            
31                    (0002)              E     A     1000  FULL 
32                    (0002)              E     A     1000  FULL 
33                    (0002)              E     R                            
34                    (0002)              E     R                            
35                    (0002)              E     R                            
36                    (0002)              E     R                            
37                    (0002)              E     R                            
38                    (0002)              E     R                            
39                                        E     R                            
40                    V5                  E     R                            
41                    V5                  E     A     100   FULL 
42                    V250                E     A     100   FULL 
43                    (0004)              E     A     1000  FULL 
44                    (0004)              E     A     1000  FULL 
45                    V5                  E     A     1000  FULL 
46                    V5                  E     A     1000  FULL 
47                    V6                  E     A     1000  FULL 
48                    V6                  E     A     1000  FULL 
49                                        E     NP                           
50                                        E     NP                           
==================================================================
   Port State: D-Disabled, E-Enabled
   Link State: A-Active, R-Ready, NP-Port not present, L-Loopback,
               D-ELSM enabled but not up
               d-Ethernet OAM enabled but not up


