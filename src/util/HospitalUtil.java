package util;

import java.util.Map;
import java.util.TreeMap;

public class HospitalUtil {
	private static HospitalUtil instance = new HospitalUtil();

	private HospitalUtil() {

	}

	public static HospitalUtil getInstance() {
		return instance;
	}

	public String designFileCodeToFolderCode(String designFileCode){
		Map<String,String> designFileCodeMaps =new TreeMap();
		designFileCodeMaps.put("PYNEH","PYN");
		designFileCodeMaps.put("BBHT","BBTH");
		String result=designFileCodeMaps.get(designFileCode);
		if(result!=null){
			return result;
		}else{
			return designFileCode;	
		}
		 

	}

}
