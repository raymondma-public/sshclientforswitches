package util;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

public class OpenFileTool {
	private static OpenFileTool instance=new OpenFileTool();
	private OpenFileTool(){
		
	}
	public static OpenFileTool getInstance(){
		return instance;
	}
	
	public void openFile(String path){
		
		Desktop dt = Desktop.getDesktop();
		try {
			dt.open(new File(path));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (IllegalArgumentException e1){
			e1.printStackTrace();
		}
	}
}
