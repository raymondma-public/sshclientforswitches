package util;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class FileAndFolderTool {

	public static void createAndWriteFile(String ip, String command,  String content){
		content=content.replace("\n", "\r\n");
		String outputString = "";
		System.out.println();
//		Scanner scanner = new Scanner(allString);

		String folderName = getCurrentFolderName();
		createFolderIfNotExist(".",folderName);
		createFolderIfNotExist(folderName,ip);
		
		try {
			PrintWriter writer = new PrintWriter("./"+folderName+"/"+ip+"/"+command + " - " + ip + ".txt", "UTF-8");

				writer.println(content);

			writer.close();
		} catch (Exception e) {
			// do something
		}
//		scanner.close();
	}

	private static void createFolderIfNotExist(String parent,String folderName) {
		File theDir = new File(parent+"/"+folderName);

		// if the directory does not exist, create it
		if (!theDir.exists()) {
		    System.out.println("creating directory: " + folderName);
		    boolean result = false;

		    try{
		        theDir.mkdir();
		        result = true;
		    } 
		    catch(SecurityException se){
		        //handle it
		    }        
		    if(result) {    
		        System.out.println("DIR created");  
		    }
		}
	}

	private static String getCurrentFolderName() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
		   //get current date time with Date()
		   Date date = new Date();
		   String folderName =dateFormat.format(date);
		return folderName;
	}
	
}
