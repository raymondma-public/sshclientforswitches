package GUI;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import config.Config;
import execute.Exec;
import model.Switch;
import util.HospitalUtil;
import util.OpenFileTool;

public class MainUI {

	private JFrame frame;
	private JTextField textFieldDesignNum;
	private static MainUI instance;
	private JList switchList = new JList();
	private JTextArea textArea = new JTextArea();
	private JList commandList = new JList();
	JLabel lblNewLabel = new JLabel("<current switch>"); // For showing current
															// switch
	JLabel lblCommand = new JLabel("<current command>");// For showing current
														// command
	/**
	 * Launch the application.
	 */
	Switch currentSwitch;
	private JTextField ipTextField;
	private JTextField vlanTextField;

	public String getIpString() {
		return ipTextField.getText();
	}

	public String getVlanString() {
		return vlanTextField.getText();
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainUI window = new MainUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainUI() {
		initialize();
		instance = this;
	}

	public static MainUI getInstance() {
		return instance;
	}

	/**
	 * Initialize the contents of the frame.
	 */

	private void initialize() {
		frame = new JFrame();
		frame.setAlwaysOnTop(true);
		frame.setBounds(100, 100, 718, 553);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		switchList.setModel(new AbstractListModel() {
			String[] values = new String[] { "loading" };

			public int getSize() {
				return values.length;
			}

			public Object getElementAt(int index) {
				return values[index];
			}
		});

		JScrollPane scrollPane = new JScrollPane();

		scrollPane.setViewportView(switchList);

		scrollPane.setBounds(10, 160, 115, 208);
		frame.getContentPane().add(scrollPane);
		switchList.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if (!arg0.getValueIsAdjusting()) {
					// Exec.updateMainUIIPList();

					String ip = switchList.getSelectedValue().toString();
					lblNewLabel.setText(ip);
					// lblNewLabel.setText(ip);
					Switch currentSwitch = Switch.findSwitchByIp(ip);

					MainUI.getInstance().currentSwitch = currentSwitch;

					Map<String, String> commandNResult = currentSwitch.getCommandNResultMap();
					ArrayList<String> commandsList = new ArrayList<String>(commandNResult.keySet());
					// Iterator entries = commandNResult.entrySet().iterator();
					// ArrayList<String> commandsList=new ArrayList();
					// while (entries.hasNext()) {
					// Entry thisEntry = (Entry) entries.next();
					// String command =(String) thisEntry.getKey();
					// commandsList.add(command);

					// Object value = thisEntry.getValue();
					// ...
					System.out.println("===============" + currentSwitch.getIp());
					for (String s : commandsList) {
						System.out.println(s);
					}
					updateCommandList(commandsList.toArray(new String[0]));
					// }
				}
			}
		});

		JLabel lblSwitch = new JLabel("Switch");
		lblSwitch.setBounds(10, 140, 46, 14);
		frame.getContentPane().add(lblSwitch);

		JScrollPane commandScrollPane = new JScrollPane();

		commandScrollPane.setViewportView(commandList);
		commandScrollPane.setBounds(130, 160, 107, 208);

		frame.getContentPane().add(commandScrollPane);
		commandList.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if (!arg0.getValueIsAdjusting()) {
					String command = commandList.getSelectedValue().toString();
					lblCommand.setText(command);
					String result = MainUI.getInstance().currentSwitch.getResultFromCommand(command);
					textArea.setText(result);
					// lblNewLabel.setText(ip);

					// Switch currentSwitch=Switch.findSwitchByIp(ip);
					// Map<String , String >
					// commandNResult=currentSwitch.getCommandNResultMap();
					// Iterator entries = commandNResult.entrySet().iterator();
					// ArrayList<String> commandsList=new ArrayList();
					// while (entries.hasNext()) {
					// Entry thisEntry = (Entry) entries.next();
					// String command =(String) thisEntry.getKey();
					// commandsList.add(command);
					//// Object value = thisEntry.getValue();
					// // ...
					// updateCommandList(commandsList.toArray(new String[0]));
					// }
				}
			}
		});

		commandList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				JList list = (JList) evt.getSource();
				if (evt.getClickCount() == 2) {
					// JOptionPane.showMessageDialog(null, "double click");
					// Double-click detected
					int index = list.locationToIndex(evt.getPoint());
					String command = commandList.getSelectedValue().toString();
					lblCommand.setText(command);
					Switch currentSwitch = MainUI.getInstance().currentSwitch;
					String result = currentSwitch.getResultFromCommand(command);

					// oneSwitch.addCommandResult("port config", outputString);
					JTextArea textArea = new JTextArea(result);
					JScrollPane scrollPane = new JScrollPane(textArea);
					textArea.setLineWrap(true);
					textArea.setWrapStyleWord(true);
					scrollPane.setPreferredSize(new Dimension(500, 800));
					// JOptionPane.showMessageDialog(null, scrollPane,
					// "dialog test with textarea",
					// JOptionPane.YES_NO_OPTION);

					JFrame demo = new JFrame(currentSwitch.getIp() + " - " + command);
					demo.setSize(500, 800);
					demo.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

					demo.getContentPane().add(BorderLayout.CENTER, scrollPane);

					demo.setVisible(true);

				} else if (evt.getClickCount() == 3) {

					// Triple-click detected
					int index = list.locationToIndex(evt.getPoint());
				}
				// }
			}
		});

		JLabel lblCommands = new JLabel("Command");
		lblCommands.setBounds(130, 140, 65, 14);
		frame.getContentPane().add(lblCommands);

		JLabel lblCurrentSwitch = new JLabel("Current Switch:");
		lblCurrentSwitch.setBounds(10, 100, 115, 14);
		frame.getContentPane().add(lblCurrentSwitch);

		JLabel lblCurrentCommand = new JLabel("Current Command:");
		lblCurrentCommand.setBounds(10, 120, 127, 14);
		frame.getContentPane().add(lblCurrentCommand);

		JLabel lblResult = new JLabel("Result:");
		lblResult.setBounds(242, 140, 46, 14);
		frame.getContentPane().add(lblResult);

		JLabel lblDesignNumber = new JLabel("Design Number:");
		lblDesignNumber.setBounds(20, 11, 96, 14);
		frame.getContentPane().add(lblDesignNumber);

		textFieldDesignNum = new JTextField();
		textFieldDesignNum.setBounds(125, 8, 107, 20);
		frame.getContentPane().add(textFieldDesignNum);
		textFieldDesignNum.setColumns(10);

		JButton btnOpenExcel = new JButton("Open Excel");
		btnOpenExcel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainUI.getInputAndOpenFiles();

			}
		});
		btnOpenExcel.setBounds(246, 7, 115, 23);
		frame.getContentPane().add(btnOpenExcel);

		JButton btnNetworkDesign = new JButton("Network Design");
		btnNetworkDesign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Desktop dt = Desktop.getDesktop();

				String designNumberString = textFieldDesignNum.getText();
				String[] designNumberSplit = designNumberString.split("_");
				String hospital = designNumberString.split("_")[0];
				String n5NDPath = Config.N5NDPATH;
				String n1NDPath = Config.N1NDPath;
				// filePath = "\\\\itip.home\\DavWWWRoot\\N3\\Network
				// Designs\\Draft Design\\Working\\"+ designNumber + ".xlsx";
				String NCSPath = Config.N5NDPATH;

				String hostpitalFileCode = designNumberSplit[0];
				String hostpiralFolderCode = HospitalUtil.getInstance().designFileCodeToFolderCode(hostpitalFileCode);

				MainUI.getInputAndOpenFiles();

				OpenFileTool.getInstance().openFile(n5NDPath + "\\" + hospital + "_ND.xls");
				OpenFileTool.getInstance().openFile(n5NDPath + "\\" + hospital + "_ND.xlsm");
				OpenFileTool.getInstance().openFile(n5NDPath + "\\" + hostpiralFolderCode + "_ND.xls");
				OpenFileTool.getInstance().openFile(n5NDPath + "\\" + hostpiralFolderCode + "_ND.xlsm");
				OpenFileTool.getInstance().openFile(n5NDPath);

				OpenFileTool.getInstance().openFile(n1NDPath + "\\" + hospital + ".ppt");
				OpenFileTool.getInstance().openFile(n1NDPath + "\\" + hostpiralFolderCode + ".ppt");
				OpenFileTool.getInstance().openFile(n1NDPath);

				OpenFileTool.getInstance().openFile(NCSPath);

			}
		});

		lblNewLabel.setBounds(130, 100, 177, 14);
		frame.getContentPane().add(lblNewLabel);

		lblCommand.setBounds(130, 120, 328, 14);
		frame.getContentPane().add(lblCommand);

		JButton btnRunCommands = new JButton("Run Commands");
		btnRunCommands.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Thread() {
					public void run() {
						Exec.main(null);

					}
				}.start();

			}
		});
		btnRunCommands.setBounds(242, 41, 140, 23);
		frame.getContentPane().add(btnRunCommands);

		JButton btnIps = new JButton("IPs");
		btnIps.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Desktop.getDesktop().edit(new File("ips.txt"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, "ips.txt?");
					e.printStackTrace();
				}
			}
		});
		btnIps.setBounds(392, 41, 89, 23);
		frame.getContentPane().add(btnIps);

		JCheckBox chckbxShowAllFrames = new JCheckBox("Show All Frames");
		chckbxShowAllFrames.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				Exec.setShowAllFrames(chckbxShowAllFrames.isSelected());
			}
		});
		chckbxShowAllFrames.setBounds(555, 67, 127, 23);
		Exec.setShowAllFrames(chckbxShowAllFrames.isSelected());

		frame.getContentPane().add(chckbxShowAllFrames);

		JButton btnReservePort = new JButton("Reserve Port");
		btnReservePort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String reservePortPath = Config.RESERVE_PORT_PATH;
				String portTablePath = Config.PORT_TABLE_PATH;
				// String NCSPath =
				// "C:\\Users\\mmh972\\Desktop\\NetworkConfig.mde";

				String NCSPath = Config.NCSPath;
				OpenFileTool.getInstance().openFile(reservePortPath);
//				OpenFileTool.getInstance().openFile(portTablePath);
				OpenFileTool.getInstance().openFile(NCSPath);
			}
		});
		btnReservePort.setBounds(10, 379, 119, 23);
		frame.getContentPane().add(btnReservePort);

		JButton btnReserveVr = new JButton("Reserve VR");
		btnReserveVr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String reserveVRPath = Config.RESERVE_VR_PATH;
				OpenFileTool.getInstance().openFile(reserveVRPath);

			}
		});
		btnReserveVr.setBounds(130, 379, 107, 23);
		frame.getContentPane().add(btnReserveVr);

		JButton btnDpReq = new JButton("DP Req");
		btnDpReq.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String designFilePath = Config.DESIGN_FILE_PATH;
				OpenFileTool.getInstance().openFile(designFilePath);
				// String pmPath = "C:\\Users\\mmh972\\Desktop\\PM(for
				// PP).accdb";
				String pmPath = Config.PM_PATH;
				OpenFileTool.getInstance().openFile(pmPath);
			}
		});
		btnDpReq.setBounds(10, 413, 89, 23);
		frame.getContentPane().add(btnDpReq);

		JButton btnServerFarmUpdate = new JButton("Server Farm Update");
		btnServerFarmUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String NCSPath = Config.NCSPath;
				String serverFarmExcelFolder = Config.SERVER_FARM_EXCEL_FOLDER_PATH;

				OpenFileTool.getInstance().openFile(NCSPath);
				OpenFileTool.getInstance().openFile(serverFarmExcelFolder);
			}
		});
		btnServerFarmUpdate.setBounds(10, 447, 164, 23);
		frame.getContentPane().add(btnServerFarmUpdate);

		JButton btnTimeOff = new JButton("Time off");
		btnTimeOff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String timeOffPath = Config.TIME_OFF_PATH;
				OpenFileTool.getInstance().openFile(timeOffPath);
			}
		});
		btnTimeOff.setBounds(10, 481, 89, 23);
		frame.getContentPane().add(btnTimeOff);

		JCheckBox chckbxAlwaysTop = new JCheckBox("Always top");
		chckbxAlwaysTop.setSelected(true);
		chckbxAlwaysTop.setBounds(555, 41, 109, 23);

		chckbxAlwaysTop.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				frame.setAlwaysOnTop(chckbxAlwaysTop.isSelected());
			}
		});
		frame.getContentPane().add(chckbxAlwaysTop);

		JButton btnRestart = new JButton("Restart");
		btnRestart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator
						+ "java";
				File currentJar;
				try {
					currentJar = new File(MainUI.class.getProtectionDomain().getCodeSource().getLocation().toURI());

					/* is it a jar file? */
					if (!currentJar.getName().endsWith(".jar"))
						return;

					/* Build command: java -jar application.jar */
					final ArrayList<String> command = new ArrayList<String>();
					command.add(javaBin);
					command.add("-jar");
					command.add(currentJar.getPath());

					final ProcessBuilder builder = new ProcessBuilder(command);
					builder.start();
					System.exit(0);
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});
		btnRestart.setBounds(604, 7, 89, 23);
		frame.getContentPane().add(btnRestart);

		JButton btnCommands = new JButton("Commands");
		btnCommands.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Desktop.getDesktop().edit(new File("commands.txt"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, "ips.txt?");
					e.printStackTrace();
				}

			}
		});

		// textArea.setBounds(20, 23, 451, 232);
		// frame.getContentPane().add(textArea);
		// textArea.setBounds(182, 296, 295, 171);
		// JTextArea textArea = new JTextArea();
		JScrollPane scrollPane_1 = new JScrollPane(textArea);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		// scrollPane.setPreferredSize(new Dimension(500, 100));
		scrollPane_1.setBounds(242, 160, 451, 340);
		frame.getContentPane().add(scrollPane_1);
		btnCommands.setBounds(392, 75, 107, 23);
		frame.getContentPane().add(btnCommands);

		JLabel lblIps = new JLabel("Ips:");
		lblIps.setBounds(53, 45, 46, 14);
		frame.getContentPane().add(lblIps);

		JLabel lblVlan = new JLabel("vlan:");
		lblVlan.setBounds(53, 75, 46, 14);
		frame.getContentPane().add(lblVlan);

		ipTextField = new JTextField();
		ipTextField.setBounds(130, 39, 86, 20);
		frame.getContentPane().add(ipTextField);
		ipTextField.setColumns(10);

		vlanTextField = new JTextField();
		vlanTextField.setBounds(130, 69, 86, 20);
		frame.getContentPane().add(vlanTextField);
		vlanTextField.setColumns(10);

		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Exec.updateMainUIIPList();
			}
		});
		btnRefresh.setBounds(604, 131, 89, 23);
		frame.getContentPane().add(btnRefresh);

		btnNetworkDesign.setBounds(371, 7, 128, 21);
		frame.getContentPane().add(btnNetworkDesign);
	}

	public void updateIPList(String[] ips) {
		Arrays.sort(ips);
		DefaultListModel ipsModel = new DefaultListModel();
		// JOptionPane.showMessageDialog(null, ips.length);
		for (String ip : ips) {
			// JOptionPane.showMessageDialog(null, ip);
			ipsModel.addElement(ip);
		}

		// switchList=new JList(ipsModel);

		switchList.setModel(ipsModel);
		textArea.setText("");

	}

	private void updateCommandList(String[] commands) {
		DefaultListModel ipsModel = new DefaultListModel();
		// JOptionPane.showMessageDialog(null, ips.length);
		for (String ip : commands) {
			// JOptionPane.showMessageDialog(null, ip);
			ipsModel.addElement(ip);
		}

		// switchList=new JList(ipsModel);

		commandList.setModel(ipsModel);
		textArea.setText("");
	}

	public static void getInputAndOpenFiles() {
		// String designNumber=;
		String designNumber = MainUI.getInstance().textFieldDesignNum.getText().trim();
		String designNumberSplit[] = designNumber.split("_");
		String hostpitalFileCode = designNumberSplit[0];
		String hostpiralFolderCode = HospitalUtil.getInstance().designFileCodeToFolderCode(hostpitalFileCode);
		// String filePath="\\\\itip.home\\DavWWWRoot\\N3\\Network
		// Designs\\PYN\\PYNEH_0242_1.xlsx";
		String filePath = Config.DRAFT_DESIGN_FOLDER_PATH + designNumber + ".xlsx";

		Desktop dt = Desktop.getDesktop();

		String portTablePath = Config.PORT_TABLE_PATH;

		String pmPath = Config.PM_PATH;
		int needPortTable = JOptionPane.showConfirmDialog(null, "Need NCS?");
		if (needPortTable == JOptionPane.YES_OPTION) {
			try {
//				dt.open(new File(portTablePath));
				dt.open(new File(Config.NCSPath));
				dt.open(new File(pmPath));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		try {
			dt.open(new File(filePath));
		} catch (IOException e1) {

			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalArgumentException e1) {
			filePath = Config.NETWORK_DESIGN_FOLDER_PATH + hostpiralFolderCode + "\\" + designNumber + ".xlsx";
			try {
				dt.open(new File(filePath));
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (IllegalArgumentException e2) {

				filePath = Config.NETWORK_DESIGN_FOLDER_PATH + "\\GOPCs\\" + hostpiralFolderCode + "\\" + designNumber
						+ ".xlsx";
				try {
					dt.open(new File(filePath));
				} catch (IOException e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				} catch (IllegalArgumentException e4) {

					e4.printStackTrace();
				}

				e2.printStackTrace();
			}
			e1.printStackTrace();
		}

		filePath = Config.NETWORK_DESIGN_FOLDER_PATH + "\\Working\\" + designNumber + ".xlsx";
		try {
			dt.open(new File(filePath));
		} catch (IOException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		} catch (IllegalArgumentException e4) {

			e4.printStackTrace();
		}
	}
}
