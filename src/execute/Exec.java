package execute;

/* -*-mode:java; c-basic-offset:2; indent-tabs-mode:nil -*- */
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;

import GUI.MainUI;
import config.Command;
import loader.IpsLoader;
import model.Port;
import model.Switch;
import util.FileAndFolderTool;

public class Exec {

	private static boolean needCheckEwanACL = false;
	private static boolean needCheckTrunkACL = true;

	private static String vlan = "";
	public static boolean showAllFrames = true;

	public static boolean isShowAllFrames() {
		return showAllFrames;
	}

	public static void setShowAllFrames(boolean showAllFrames) {
		Exec.showAllFrames = showAllFrames;
	}

	public static void main(String[] arg) {

		// getInputAndOpenFiles();

		try {
			JSch jsch = new JSch();

			String host = null;

			String user = "mmh972";
			ArrayList<String> switchIps = new ArrayList();

			System.out.print("IPs:");

			String ips = MainUI.getInstance().getIpString();
			String vlan = MainUI.getInstance().getVlanString();
			Exec.vlan = vlan;
			if (ips.contains("&")) { // add N switch with last octect different
				IpsLoader.addNSwitchWithDiffLastOct(switchIps, ips);
			} else if (ips.contains("|")) { // add N switch with whole ip
				addNSwitchWithWholeIp(switchIps, ips);
			} else if (ips.equals("")) { // add N switches from file with whole
											// ip
				addSwitchesFromFile(switchIps);
			} else {

				switchIps.add(ips);
			}

			ArrayList<Thread> threads = new ArrayList();
			for (String ip : switchIps) {
				createThreadFor1Switch(jsch, user, Switch.switches, threads, ip);
			} // for every switch

			System.out.println("Finish getting");

			for (Thread t : threads) {
				t.join();
				updateMainUIIPList();
			}

			ArrayList<Thread> uiThreads = new ArrayList();
			for (Switch oneSwitch : Switch.switches) {
				createAndStartUIThread1Switch(uiThreads, oneSwitch);
				// oneSwitch.displayPortNoEsrp();
				oneSwitch.displayPortNoEsrpAndSharing();
				ArrayList<String> portNoEsrpNoShared = oneSwitch.getPortNoEsrpNoShared();

				oneSwitch.addCommandResult("PortNoEsrpAndSharing", Arrays.toString(portNoEsrpNoShared.toArray()));
				FileAndFolderTool.createAndWriteFile(oneSwitch.getIp(), "PortNoEsrpAndSharing", Arrays.toString(portNoEsrpNoShared.toArray()));
			}

			for (Thread t : uiThreads) {
				t.join();
			}

			JOptionPane.showMessageDialog(null, "Finished");

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	private static void createAndStartUIThread1Switch(ArrayList<Thread> uiThreads, Switch oneSwitch) {
		Thread currentThread = new Thread() {

			public void run() {
				String outputString = "";
				outputString += oneSwitch.getIp() + "\n";
				System.out.println(oneSwitch.getIp());
				for (Port port : oneSwitch.getPorts()) {
					System.out.print(port.getPortNumber() + "\t");
					outputString += port.getPortNumber() + "\t";

					if (port.getStatus().contains("R")) {
						System.out.print("    ");
						outputString += "        ";
					}
					System.out.println(port.getStatus());
					outputString += port.getStatus() + "\n";
				}

				// JOptionPane.showMessageDialog(null, new
				// JTextArea(outputString));
				oneSwitch.addCommandResult("port config", outputString);
				FileAndFolderTool.createAndWriteFile(oneSwitch.getIp(), "port config", outputString);
				JTextArea textArea = new JTextArea(outputString);
				JScrollPane scrollPane = new JScrollPane(textArea);
				textArea.setLineWrap(true);
				textArea.setWrapStyleWord(true);
				scrollPane.setPreferredSize(new Dimension(500, 800));
				// JOptionPane.showMessageDialog(null, scrollPane,
				// "dialog test with textarea",
				// JOptionPane.YES_NO_OPTION);

				if (Exec.showAllFrames) {
					JFrame demo = new JFrame(oneSwitch.getIp());
					demo.setSize(500, 800);
					demo.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

					demo.getContentPane().add(BorderLayout.CENTER, scrollPane);

					demo.setVisible(true);
				}
			}
		};
		uiThreads.add(currentThread);
		currentThread.start();
	}

	private static void createThreadFor1Switch(JSch jsch, String user, ArrayList<Switch> switches,
			ArrayList<Thread> threads, String ip) {
		Thread currentThread = new Thread() {

			public void run() {
				ArrayList<String> commands = new ArrayList();
				try {

					if (needCheckEwanACL) {
						System.out.println("IP:" + ip);
						Switch currentSwitch = new Switch(ip);
						Session session = jsch.getSession(user, ip, 22);

						UserInfo ui = new MyUserInfo();
						session.setUserInfo(ui);

						session.connect();
						System.out.println("connect");
						ArrayList<String> preCommands = new ArrayList();
						preCommands.add("sh hkt");
						preCommands.add("sh hgc");

						for (String command : preCommands) {
							String allString = openChannelRunOneCommand(session, command);
							ArrayList<Port> ewanPorts = checkEwanCommand(ip, allString, session, command);
							for (Port p : ewanPorts) {
								commands.add("show access-list port " + p.getPortNumber());
							}
						}
						session.disconnect();
					}

					commands.add(Command.CMD_SHOW_PORT_CONFIG_N);
//
//					commands.add(Command.CMD_SHOW_ESRP);
//					commands.add(Command.CMD_SHOW_SHARING);

					// commands.add("sh vlan");
					commands.add("sh v" + Exec.vlan);
					
					Scanner commandReader = new Scanner(new File("commands.txt"));
					while (commandReader.hasNextLine()) {
						commands.add(commandReader.nextLine());
					}
					commandReader.close();
					
					
					
					// commands.add("sh hkt");
					// commands.add("sh hgc");
					// commands.add("sh access-list port 6:47");
					runAndInitOneSwitch(jsch, user, switches, ip, commands);

				} catch (JSchException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		currentThread.start();
		threads.add(currentThread);
	}

	private static void addNSwitchWithWholeIp(ArrayList<String> switchIps, String ips) {
		System.out.println(ips);
		ips = ips.replaceAll(" ", "");

		String allIps[] = ips.split(",");
		for (String ip : allIps) {
			System.out.println(ip);
			// switchIps.add(ip);
		}

		for (String ip : allIps) {
			// System.out.println(ip);
			switchIps.add(ip);
		}
	}

	private static void addSwitchesFromFile(ArrayList<String> switchIps) throws FileNotFoundException {
		Scanner fileScanner = new Scanner(new File("ips.txt"));
		while (fileScanner.hasNextLine()) {
			switchIps.add(fileScanner.nextLine());
		}
	}

	private static void runAndInitOneSwitch(JSch jsch, String user, ArrayList<Switch> switches, String ip,
			ArrayList<String> commands) throws JSchException, IOException {
		System.out.println("IP:" + ip);
		Switch currentSwitch = new Switch(ip);
		Session session = jsch.getSession(user, ip, 22);
		// commands.add("sh access-list port 6:47");

		/*
		 * String xhost="127.0.0.1"; int xport=0; String
		 * display=JOptionPane.showInputDialog("Enter display name",
		 * xhost+":"+xport); xhost=display.substring(0, display.indexOf(':'));
		 * xport=Integer.parseInt(display.substring(display.indexOf(':') +1)) ;
		 * session.setX11Host(xhost); session.setX11Port(xport+6000);
		 */

		// username and password will be given via UserInfo interface.
		UserInfo ui = new MyUserInfo();
		session.setUserInfo(ui);

		// commands.add("sh port n");
		// commands.add("sh port n");

		session.connect();
		System.out.println("connect");
		ArrayList<String> newCommands = new ArrayList();
		ArrayList<Thread> threads = new ArrayList();

		for (int i = 0; i < commands.size(); i++) {
			String command = commands.get(i);
			System.out.println("=============Run Command: " + command);
			String allString = openChannelRunOneCommand(session, command);
			if (command.contains(Command.CMD_SHOW_PORT_CONFIG_N)) { // init
																	// status
				Thread currentThread = new Thread() {
					public void run() {

						initOneSwitchStatus(currentSwitch, allString);

					}
				};
				currentThread.start();
				threads.add(currentThread);

			} else if (command.contains("sh v" + Exec.vlan)) {
				// JOptionPane.showMessageDialog(null, "show vlan");
				Thread currentThread = new Thread() {

					public void run() {

						showVlanCommand(currentSwitch, allString);

					}
				};
				currentThread.start();
				threads.add(currentThread);

			} else if (command.contains(Command.CMD_SHOW_HKT) || command.contains(Command.CMD_SHOW_HGC)) {
				Thread currentThread = new Thread() {

					public void run() {

						ArrayList<Port> ewanPorts = checkEwanCommand(ip, allString, session, command);
						JOptionPane.showMessageDialog(null, ewanPorts.size());
						for (Port p : ewanPorts) {
							newCommands.add("sh access-list port " + p.getPortNumber());
						}

					}
				};
				currentThread.start();
				threads.add(currentThread);

			} else if (command.contains(Command.CMD_SHOW_ESRP)) {

				shEsrpCommand(currentSwitch, allString);

			} else if (command.contains(Command.CMD_SHOW_SHARING)) {

				shSharingCommand(currentSwitch, allString);
			} else if (command.contains("show access-list port")) {

				String returnString = "";
				System.out.println();
				Scanner scanner = new Scanner(allString);
				while (scanner.hasNextLine()) {
					String currentLine = scanner.nextLine();
					System.out.println(currentLine);
					returnString += currentLine + "\n";
				}
				scanner.close();
				String outputString = "";
				outputString += "ip:\t" + ip + "\n";
				outputString += "port: " + command.replace("show access-list port", "") + "\t numberOfLine:\t"
						+ countLines(returnString) + "\n" + returnString;

				// Switch.findSwitchByIp(ip).addCommandResult("show access-list
				// port", outputString);

				if (countLines(returnString) != 0) {
					JTextArea textArea = new JTextArea(outputString);
					JScrollPane scrollPane = new JScrollPane(textArea);
					textArea.setLineWrap(true);
					textArea.setWrapStyleWord(true);
					scrollPane.setPreferredSize(new Dimension(500, 100));

					if (Exec.showAllFrames) {
						JFrame demo = new JFrame(ip);
						demo.setSize(500, 100);
						demo.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

						demo.getContentPane().add(BorderLayout.CENTER, scrollPane);
						Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
						demo.setLocation(dim.width - demo.getSize().width, 0);
						demo.setVisible(true);
					}
				}
			} else {
				String outputString = "";
				System.out.println();
				Scanner scanner = new Scanner(allString);
				while (scanner.hasNextLine()) {
					String currentLine = scanner.nextLine();
					System.out.println(currentLine);
					outputString += currentLine+"\n";
				}
				scanner.close();
				currentSwitch.addCommandResult(command, outputString);
				FileAndFolderTool.createAndWriteFile(currentSwitch.getIp(), command, outputString);
//				JOptionPane.showMessageDialog(null, outputString);
			}

		} // for every command
		for (Thread t : threads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		session.disconnect();

		switches.add(currentSwitch);

	}

	public static void updateMainUIIPList() {
		ArrayList<String> ipsList = new ArrayList();
		// JOptionPane.showMessageDialog(null, switches.size());
		for (Switch s : Switch.switches) {
			ipsList.add(s.getIp());
		}
		MainUI.getInstance().updateIPList((String[]) ipsList.toArray(new String[0]));
	}

	private static void shSharingCommand(Switch currentSwitch, String allString) {
		boolean startedRecording = false;
		int lineCount = -1;
		int seperatorCount = 0;
		Scanner scanner = new Scanner(allString);

		String sharingReport = "";
		String okString = "";
		String notOkString = "";
		String exceptionCase = "";

		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			sharingReport += currentLine + "\n";
			if (currentLine
					.contains("Master    Master     Control   Algorithm   Group     Mbr   State   Transitions")) {
				startedRecording = true;
				lineCount = -1;
			}
			if (currentLine.contains("============================================================================")) {
				seperatorCount++;
				if (seperatorCount == 2) {
					break;
				}
			}

			System.out.println(currentLine);

			if (startedRecording && lineCount >= 1) {
				Scanner lineScanner = new Scanner(currentLine);
				// sharingReport+=
				String sharingPort = lineScanner.next().trim();
				// System.out.println("sharingPort: "+sharingPort);
				if (Character.isDigit(sharingPort.charAt(0))) {
					if (currentSwitch.containEsrpPort(sharingPort)) {
						// JOptionPane.showMessageDialog(null, sharingPort);
						okString += sharingPort + " , ";
						currentSwitch.addSharingPortsWithEsrp(sharingPort);
					} else {
						notOkString += sharingPort + " , ";
						currentSwitch.addSharingPortsWithoutEsrp(sharingPort);
						// JOptionPane.showMessageDialog(null, "NO!!!"+
						// sharingPort);
					}
				} else {
					exceptionCase += sharingPort + " , ";
				}
				// if (scanner.hasNextLine()) {
				// scanner.nextLine();
				// }

				lineScanner.close();

			}
			lineCount++;
		}

		JTextArea textArea = new JTextArea(sharingReport + "\nExceptions: " + exceptionCase + "\nOK: " + okString
				+ "\tNot OK: " + notOkString + "\t#Esrp record:" + currentSwitch.getEsrpRecordsNumber());

		currentSwitch.addCommandResult("shSharingCommand", sharingReport + "\nExceptions: " + exceptionCase + "\nOK: "
				+ okString + "\tNot OK: " + notOkString + "\t#Esrp record:" + currentSwitch.getEsrpRecordsNumber());
		
		FileAndFolderTool.createAndWriteFile(currentSwitch.getIp(), "shSharingCommand",  sharingReport + "\nExceptions: " + exceptionCase + "\nOK: "
				+ okString + "\tNot OK: " + notOkString + "\t#Esrp record:" + currentSwitch.getEsrpRecordsNumber());
		
		JScrollPane scrollPane = new JScrollPane(textArea);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrollPane.setPreferredSize(new Dimension(500, 800));
		// JOptionPane.showMessageDialog(null, scrollPane,
		// "dialog test with textarea",
		// JOptionPane.YES_NO_OPTION);

		if (Exec.showAllFrames) {
			JFrame demo = new JFrame(currentSwitch.getIp());
			demo.setSize(500, 800);
			demo.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

			demo.getContentPane().add(BorderLayout.CENTER, scrollPane);
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			demo.setLocation(dim.width / 2, dim.height / 2 - demo.getSize().height / 2);// -
																						// demo.getSize().width
																						// /
																						// 2
			demo.setVisible(true);
		}
		// JOptionPane.showMessageDialog(null, "OK: "+okString+"\nNot OK:
		// "+notOkString);
		scanner.close();
		//// JOptionPane.showMessageDialog(null, "end");
	}

	private static void shEsrpCommand(Switch currentSwitch, String allString) {
		Scanner scanner = new Scanner(allString);
		boolean startedRecording = false;
		int lineCount = -1;
		while (scanner.hasNextLine()) {

			String currentLine = scanner.nextLine();
			System.out.println(currentLine);

			if (currentLine.contains("Port	  Weight	Host	Restart")) {
				startedRecording = true;

			}

			String datas[] = currentLine.split(" ");

			if (lineCount >= 1) {
				currentSwitch.addEsrpPort(datas[0]);
				System.out.println(datas[0]);
			}
			if (startedRecording) {
				lineCount++;
			}
		}
		// JOptionPane.showMessageDialog(null, currentSwitch.getIp()+"esrp #: "+
		// currentSwitch.getEsrpRecordsNumber());
		scanner.close();
	}

	private static ArrayList<Port> checkEwanCommand(String ip, String allString, Session session, String command) {
		boolean startRecording = false;
		String untagPortStrings = "";
		Scanner scanner = new Scanner(allString);
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			System.out.println(currentLine);

			if (currentLine.contains("Untag:")) {
				startRecording = true;
			}
			if (currentLine.contains("Tag:")) {
				startRecording = false;
				break;
			}
			if (currentLine.contains("Flags:")) {
				startRecording = false;
				break;
			}
			if (startRecording == true) {
				untagPortStrings += currentLine;
			}

		}

		System.out.println("untagPortStrings: " + untagPortStrings);

		untagPortStrings = untagPortStrings.replaceAll("Untag:", "");
		untagPortStrings = untagPortStrings.trim();
		String allUntagPortsForThisSwitch[] = untagPortStrings.split(",");

		ArrayList<Port> ewanPorts = new ArrayList();
		String portsCanBeUsed = "";
		for (String portString : allUntagPortsForThisSwitch) {
			if (portString.contains("*")) {
				portsCanBeUsed += portString + ", ";
				ewanPorts.add(new Port(portString.replace("*", ""), "A"));
			}
		}

		for (Port p : ewanPorts) {
			try {
				String resultString = openChannelRunOneCommand(session, "show access-list port " + p.getPortNumber());
				// JOptionPane.showMessageDialog(null, ip+" #line:
				// "+countLines(resultString));
			} catch (JSchException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// JOptionPane.showMessageDialog(null, portsCanBeUsed);

		JTextArea textArea = new JTextArea(portsCanBeUsed);
		JScrollPane scrollPane = new JScrollPane(textArea);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrollPane.setPreferredSize(new Dimension(500, 100));
		// JOptionPane.showMessageDialog(null, scrollPane,
		// "dialog test with textarea",
		// JOptionPane.YES_NO_OPTION);

		if (Exec.showAllFrames) {
			JFrame demo = new JFrame(ip);
			demo.setSize(500, 100);
			demo.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

			demo.getContentPane().add(BorderLayout.CENTER, scrollPane);

			demo.setVisible(true);
		}
		return ewanPorts;
	}

	private static void showVlanCommand(Switch currentSwitch, String allString) {
		boolean startRecording = false;
		String untagPortStrings = "";
		Scanner scanner = new Scanner(allString);
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			System.out.println(currentLine);

			if (currentLine.contains("Untag:")) {
				startRecording = true;
			}
			if (currentLine.contains("Tag:")) {
				startRecording = false;
				break;
			}
			if (startRecording == true) {
				untagPortStrings += currentLine;
			}

		}

		System.out.println("untagPortStrings: " + untagPortStrings);

		untagPortStrings = untagPortStrings.replaceAll("Untag:", "");
		untagPortStrings = untagPortStrings.trim();
		String allUntagPortsForThisSwitch[] = untagPortStrings.split(",");

		String portsCanBeUsed = "";
		for (String portString : allUntagPortsForThisSwitch) {
			if (!portString.contains("*")) {
				portsCanBeUsed += portString + ", ";

			}
		}
		// JOptionPane.showMessageDialog(null, "show Vlan");
		// JOptionPane.showMessageDialog(null, portsCanBeUsed);
		System.out.println("show Vlan: ");
		System.out.println(portsCanBeUsed);

		currentSwitch.addCommandResult("show Vlan", portsCanBeUsed);
		FileAndFolderTool.createAndWriteFile(currentSwitch.getIp(), "show Vlan", portsCanBeUsed);

		JTextArea textArea = new JTextArea(portsCanBeUsed);
		JScrollPane scrollPane = new JScrollPane(textArea);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrollPane.setPreferredSize(new Dimension(500, 100));
		// JOptionPane.showMessageDialog(null, scrollPane,
		// "dialog test with textarea",
		// JOptionPane.YES_NO_OPTION);
		if (Exec.showAllFrames) {
			JFrame demo = new JFrame(currentSwitch.getIp());
			demo.setSize(500, 100);
			demo.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

			demo.getContentPane().add(BorderLayout.CENTER, scrollPane);

			demo.setVisible(true);
		}
	}

	private static void initOneSwitchStatus(Switch currentSwitch, String allString) {
		int portNumber = 0;
		boolean startRecord = false;
		int lineCount = 0;

		Scanner scanner = new Scanner(allString);
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			if (currentLine
					.contains("         router     State State Neg  Cfg Actual Cfg Actual Cntrl Master Pri Red")) {
				portNumber = -1;
				startRecord = true;
			}
			if (currentLine
					.contains("==============================================================================")) {
				lineCount++;
			}

			if (startRecord) {
				if (lineCount == 2) {
					break;
				}

				if (portNumber > 0) {
					System.out.println(currentLine);
					Scanner lineScanner = new Scanner(currentLine);
					String portName = lineScanner.next();
					lineScanner.next();// useless parameter
					lineScanner.next();// useless parameter
					String portStatus = lineScanner.next();
					currentSwitch.addPort(portName, portStatus);
				}
				portNumber++;
			}

		}
		scanner.close();
	}

	private static String openChannelRunOneCommand(Session session, String command) throws JSchException, IOException {
		// String command=JOptionPane.showInputDialog("Enter
		// command",
		// "set|grep SSH");

		Channel channel = session.openChannel("exec");
		((ChannelExec) channel).setCommand(command);

		// X Forwarding
		// channel.setXForwarding(true);

		// channel.setInputStream(System.in);
		channel.setInputStream(null);

		// channel.setOutputStream(System.out);

		// FileOutputStream fos=new FileOutputStream("/tmp/stderr");
		// ((ChannelExec)channel).setErrStream(fos);
		((ChannelExec) channel).setErrStream(System.err);

		InputStream in = channel.getInputStream();

		channel.connect();

		String allString = "";
		byte[] tmp = new byte[1024];
		while (true) {
			while (in.available() > 0) {
				int i = in.read(tmp, 0, 1024);
				if (i < 0)
					break;
				// System.out.print(new String(tmp, 0, i));
				allString += new String(tmp, 0, i);
			}
			if (channel.isClosed()) {
				if (in.available() > 0)
					continue;
				// System.out.println("exit-status: " +
				// channel.getExitStatus());
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (Exception ee) {
			}
		}

		channel.disconnect();
		return allString;
	}

	public static class MyUserInfo implements UserInfo, UIKeyboardInteractive {
		public String getPassword() {
			return passwd;
		}

		public boolean promptYesNo(String str) {
			// Object[] options = { "yes", "no" };
			// int foo = JOptionPane.showOptionDialog(null, str, "Warning",
			// JOptionPane.DEFAULT_OPTION,
			// JOptionPane.WARNING_MESSAGE, null, options, options[0]);
			// return foo == 0;
			return true;
		}

		String passwd;
		JTextField passwordField = (JTextField) new JPasswordField(20);

		public String getPassphrase() {
			return null;
		}

		public boolean promptPassphrase(String message) {
			return true;
		}

		public boolean promptPassword(String message) {
			passwd = "H033123i";
			return true;
			// Object[] ob={passwordField};
			// int result=
			// JOptionPane.showConfirmDialog(null, ob, message,
			// JOptionPane.OK_CANCEL_OPTION);
			// if(result==JOptionPane.OK_OPTION){
			// passwd=passwordField.getText();
			// return true;
			// }
			// else{
			// return false;
			// }
		}

		public void showMessage(String message) {
			JOptionPane.showMessageDialog(null, message);
		}

		final GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0);
		private Container panel;

		public String[] promptKeyboardInteractive(String destination, String name, String instruction, String[] prompt,
				boolean[] echo) {
			panel = new JPanel();
			panel.setLayout(new GridBagLayout());

			gbc.weightx = 1.0;
			gbc.gridwidth = GridBagConstraints.REMAINDER;
			gbc.gridx = 0;
			panel.add(new JLabel(instruction), gbc);
			gbc.gridy++;

			gbc.gridwidth = GridBagConstraints.RELATIVE;

			JTextField[] texts = new JTextField[prompt.length];
			for (int i = 0; i < prompt.length; i++) {
				gbc.fill = GridBagConstraints.NONE;
				gbc.gridx = 0;
				gbc.weightx = 1;
				panel.add(new JLabel(prompt[i]), gbc);

				gbc.gridx = 1;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.weighty = 1;
				if (echo[i]) {
					texts[i] = new JTextField(20);
				} else {
					texts[i] = new JPasswordField(20);
				}
				panel.add(texts[i], gbc);
				gbc.gridy++;
			}

			if (JOptionPane.showConfirmDialog(null, panel, destination + ": " + name, JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION) {
				String[] response = new String[prompt.length];
				for (int i = 0; i < prompt.length; i++) {
					response[i] = texts[i].getText();
				}
				return response;
			} else {
				return null; // cancel
			}
		}
	}

	private static int countLines(String str) {
		String[] lines = str.split("\r\n|\r|\n");
		return lines.length;
	}
}