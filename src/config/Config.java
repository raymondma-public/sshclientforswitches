package config;

public class Config {
	public static String N5NDPATH="\\\\corp.ha.org.hk\\ha\\HO\\FD\\IT\\TOSS\\COMDATA\\NWSTATUS.OPN\\NW.DIG";;
	public static String N1NDPath = "\\\\corp.ha.org.hk\\ha\\HO\\FD\\IT\\NMS\\COMDATA\\Configuration Management\\N1\\Network Diagram";
	public static String NCSPath = "\\\\Corp.ha.org.hk\\ha\\HO\\FD\\IT\\NMS\\N3\\User\\PP\\Sharing area\\DBs\\NetworkConfig.mde";
	public static String RESERVE_PORT_PATH = "\\\\itip.home\\DavWWWRoot\\N3\\Network Designs\\Draft Design\\Working\\A1 RedeployList.xlsx";
	public static String PORT_TABLE_PATH="\\\\corp.ha.org.hk\\HA\\HO\\FD\\IT\\NMS\\N3\\Network Implementation\\Network Implementation\\Building Switch Ports\\L3 To L2 switch port assignment (Available).xlsx";
	public static String RESERVE_VR_PATH="\\\\corp.ha.org.hk\\HA\\HO\\FD\\IT\\NMS\\N3\\Network Implementation\\Network Implementation\\Building Switch Ports\\MNI core switch VR.xlsx";
	public static String DESIGN_FILE_PATH="\\\\corp.ha.org.hk\\HA\\HO\\FD\\IT\\NMS\\N3\\Network Design\\Design Standard Form\\L2 Design Template v4.xltx";
	public static String PM_PATH="\\\\Corp.ha.org.hk\\ha\\HO\\FD\\IT\\NMS\\N3\\User\\PP\\Sharing area\\DBs\\PM(for PP).accdb";
	public static String SERVER_FARM_EXCEL_FOLDER_PATH="\\\\corp.ha.org.hk\\HA\\HO\\FD\\IT\\NMS\\COMDATA\\Configuration Management\\N3\\Server Farm Ports";
	public static String TIME_OFF_PATH="\\\\corppyndc01\\ha\\HO\\FD\\IT\\NMS\\N3\\Comdata\\Team Administration\\OT & Annual Leave\\Monthly Report.xlsx";
	public static String DRAFT_DESIGN_FOLDER_PATH="\\\\itip.home\\DavWWWRoot\\N3\\Network Designs\\Draft Design\\";
	public static String NETWORK_DESIGN_FOLDER_PATH="\\\\itip.home\\DavWWWRoot\\N3\\Network Designs\\" ;
}
