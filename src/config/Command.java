package config;

public class Command {
	public static final String CMD_SHOW_PORT_CONFIG_N="sh port config n";
	public static final String CMD_SHOW_SHARING="sh sharing";
	public static final String CMD_SHOW_ESRP="sh esrp";
	public static final String CMD_SHOW_HKT="sh hkt";
	public static final String CMD_SHOW_HGC="sh hgc";
	private static Command instance=new Command();
	
	private Command(){
		
	}
	public static Command getInstance(){
		return instance;	
	}
	
	
}
