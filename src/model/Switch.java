package model;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import execute.Exec;

public class Switch {

	public static ArrayList<Switch> switches = new ArrayList();
	private ArrayList<Port> ports = new ArrayList();
	private String ip = "";

	private Map<String, String> commandNResult = new TreeMap();

	public void addCommandResult(String command, String result) {
		commandNResult.put(command, result);

	}

	public String getResultFromCommand(String command) {
		return commandNResult.get(command);
	}

	public Map<String, String> getCommandNResultMap() {
		return commandNResult;
	}

	private Set<String> esrpPort = new HashSet() {
		@Override
		public boolean contains(Object arg0) {
			// TODO Auto-generated method stub
			for (String s : esrpPort) {
				// System.out.println("compare: "+s+" "+(String)arg0);
				if (s.trim().equals(((String) arg0).trim())) {
					return true;
				}
			}
			return false;
		}

	};
	private ArrayList<String> sharingPortsWithEsrp = new ArrayList() {
		@Override
		public boolean contains(Object arg0) {
			// TODO Auto-generated method stub
			for (String s : sharingPortsWithEsrp) {
				// System.out.println("compare: "+s+" "+(String)arg0);
				if (s.trim().equals(((String) arg0).trim())) {
					return true;
				}
			}
			return false;
		}

	};
	private ArrayList<String> sharingPortsWithoutEsrp = new ArrayList() {
		@Override
		public boolean contains(Object arg0) {
			// TODO Auto-generated method stub
			for (String s : sharingPortsWithoutEsrp) {
				// System.out.println("compare: "+s+" "+(String)arg0);
				if (s.trim().equals(((String) arg0).trim())) {
					return true;
				}
			}
			return false;
		}

	};

	public ArrayList<String> getSharingPortsWithoutEsrp() {
		return sharingPortsWithoutEsrp;
	}

	public void addSharingPortsWithoutEsrp(String sharingPortsWithoutEsrp) {
		this.sharingPortsWithoutEsrp.add(sharingPortsWithoutEsrp);
	}

	public ArrayList<String> getSharingPortsWithEsrp() {
		return sharingPortsWithEsrp;
	}

	public void addSharingPortsWithEsrp(String sharingPortsWithEsrp) {
		this.sharingPortsWithEsrp.add(sharingPortsWithEsrp);
	}

	public Switch(String ip) {
		this.ip = ip;
	}

	public void addPort(String portNumber, String status) {
		ports.add(new Port(portNumber, status));
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public ArrayList<Port> getPorts() {
		return ports;
	}

	public void setPorts(ArrayList<Port> ports) {
		this.ports = ports;
	}

	public void addEsrpPort(String esrpPort) {
		this.esrpPort.add(esrpPort);
	}

	public boolean containEsrpPort(String esrpPort) {

		return this.esrpPort.contains(esrpPort);
	}

	public int getEsrpRecordsNumber() {
		return esrpPort.size();
	}

	public ArrayList<String> checkPortNoEsrp() {
		ArrayList<String> resultAList = new ArrayList();
		for (Port p : ports) {
			if (!esrpPort.contains(p.getPortNumber())) {
				resultAList.add(p.getPortNumber());
			}
		}
		System.out.println(Arrays.toString(resultAList.toArray()));
		return resultAList;
	}

	public void displayPortNoEsrp() {
		ArrayList<String> noEsrps = checkPortNoEsrp();

		JTextArea textArea = new JTextArea(Arrays.toString(noEsrps.toArray()));
		JScrollPane scrollPane = new JScrollPane(textArea);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrollPane.setPreferredSize(new Dimension(500, 100));
		// JOptionPane.showMessageDialog(null, scrollPane,
		// "dialog test with textarea",
		// JOptionPane.YES_NO_OPTION);

		JFrame demo = new JFrame(ip);
		demo.setSize(500, 100);
		demo.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		demo.getContentPane().add(BorderLayout.CENTER, scrollPane);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		demo.setLocation(dim.width / 2 - demo.getSize().width / 2, 0);
		demo.setVisible(true);
	}

	public ArrayList<String> getPortNoEsrpNoShared() {
		ArrayList<String> noEsrps = checkPortNoEsrp();
		ArrayList<String> pairedPorts = sharingPortToSharedPorts(this.sharingPortsWithEsrp);
		ArrayList<String> output = new ArrayList();

		for (String s1 : noEsrps) {
			if (!pairedPorts.contains(s1)) {
				output.add(s1);
			}
		}

		return output;

	}

	public void displayPortNoEsrpAndSharing() {
		// TODO Auto-generated method stub
		ArrayList<String> portNoEsrpNoShared = getPortNoEsrpNoShared();

		JTextArea textArea = new JTextArea(Arrays.toString(portNoEsrpNoShared.toArray()));
		JScrollPane scrollPane = new JScrollPane(textArea);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrollPane.setPreferredSize(new Dimension(500, 100));
		// JOptionPane.showMessageDialog(null, scrollPane,
		// "dialog test with textarea",
		// JOptionPane.YES_NO_OPTION);

		if (Exec.showAllFrames) {
			JFrame demo = new JFrame(ip);
			demo.setSize(500, 100);
			demo.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

			demo.getContentPane().add(BorderLayout.CENTER, scrollPane);
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			demo.setLocation(dim.width / 2 - demo.getSize().width / 2, 0);
			demo.setVisible(true);
		}
	}

	private ArrayList<String> sharingPortToSharedPorts(ArrayList<String> sharingPorts) {
		ArrayList<String> output = new ArrayList();
		for (String s : sharingPorts) {
			if (s.contains(":")) {
				String twoParts[] = s.split(":");
				int portNumber = Integer.parseInt(twoParts[1]);
				int paredPortNumber = portNumber + 1;
				String paredPort = twoParts[0] + ":" + paredPortNumber;
				output.add(paredPort);

			} else {
				int portNumber = Integer.parseInt(s);
				int paredPortNumber = portNumber + 1;
				output.add("" + paredPortNumber);
			}

		}

		return output;
	}

	public static Switch findSwitchByIp(String ip) {
		for (Switch s : switches) {
			if (s.getIp().equals(ip)) {
				return s;
			}
		}
		return null;
	}
}
